<?php
    /* home.php
     * project1
     * Marian Oprisan */

    require_once('../includes/helper.php');

    if (isset($_SESSION['user_id']))
    {
        render('home');
    }
    else
    {
        render('login');
    }
?>
