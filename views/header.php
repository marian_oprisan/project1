<!DOCTYPE html>
<html>
    <head>
        <title><?= htmlspecialchars($title) ?></title>
        <link rel='stylesheet' type='text/css' href='stylesheet.css'>
        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
    </head>

    <body>
        <div id='top'>
            <h1 id='title'>C$75 Finance</h1>
            <a id='home_link' href='/'>Homepage</a><br />
        </div>
