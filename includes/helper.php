<?php
/****************************************************
 * helper.php
 *
 * CSCI S-75
 * Project 1
 * Marian Oprisan
 *
 * Renders a view template with specified parameters
 ****************************************************/

/*
 * render() - Renders the template
 * imported from cs-75 section5 source code
 * @param string $template - The name of the template to render.
 * @param array $data - An array of variables and values to pass to the template.
 */
function render($template, $data = array())
{
    $path = __DIR__ . '/../views/' . $template . '.php';
	if (file_exists($path))
    {
        extract($data);
        require($path);
    }
}

?>
